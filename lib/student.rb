class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name.capitalize
    @last_name = last_name.capitalize
    @courses = []
  end

  def name
    first_name + " " + last_name
  end

  def enroll(new_course)
    self.courses.each do |enrolled_course|
      if enrolled_course.conflicts_with?(new_course)
        raise_error "New course conflicts with already enrolled course!"
      end
    end

    unless courses.include?(new_course)
      courses << new_course
      new_course.students << self
    end
  end

  def course_load
    load = Hash.new(0)

    courses.each do |course|
      load[course.department] += course.credits
    end

    load
  end
end
